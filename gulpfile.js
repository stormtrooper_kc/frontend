'use strict'
const gulp      = require('gulp'),
    sass      = require('gulp-sass'),
    browserSync   = require('browser-sync').create(),
    browserify  = require('browserify'),
    tap       = require('gulp-tap'),
    buffer    = require('gulp-buffer'),
    babelify    = require('babelify'),
    fileinclude   = require('gulp-file-include'),
    uglify    = require('gulp-uglify'),
    sourcemaps  = require('gulp-sourcemaps'),
    postcss     = require('gulp-postcss'),
    autoprefixer  = require('autoprefixer'),
    cssnano     = require('cssnano'),
    imagemin    = require('gulp-imagemin'),
    gutil     = require('gulp-util'),
    stripDebug = require('gulp-strip-debug'),
    clean = require('gulp-clean'),
    source    = require('vinyl-source-stream'),
    htmlmin     = require('gulp-htmlmin');;


let developPath = './src/*.html';
let includePath = './src/includes/**/*.html';
let fontPath = './src/fonts/**/*.{ttf,woff,woff2,eof,svg}'
// let videoPath = './src/videos/**/*.mp4';
let sassPath = './src/scss/**/*.scss';
let jsPath = './src/js/app.js';
let assetsImg = './src/images/*.{png,jpg,jpeg,gif,svg,JPEG}';

/**
 *
 * Tareas Desarrollo
 *
 */


/* Tarea para gestionar los includes de html*/

gulp.task('include-files', function() {
  return gulp.src(developPath)
  .pipe(fileinclude({
    prefix: '@@',
    basepath: '@file'
  }))
  .pipe(htmlmin({collapseWhitespace: true}))
  .pipe(gulp.dest('./dist/'))
  .pipe(browserSync.stream());
});

/* Tarea para copiar las fuentes a dist/ */

gulp.task('copying-fonts', function() {
  return gulp.src(fontPath)
  .pipe(gulp.dest('./dist/fonts/'));
});

/* Tarea que genera el css a traves de sass */

gulp.task('compile-sass', function() {
  return gulp.src(sassPath)
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', sass.logError))
  .pipe(postcss([autoprefixer(), cssnano()]))
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest('./dist/css/'))
  .pipe(browserSync.stream());
});

/* Tarea que transpila el javascript para navegador con browserify */

gulp.task('browserify-js-files', function() {
  let b = browserify({
    entries: './src/js/app.js',
    debug: true
  })
  .transform(babelify);

  return b.bundle()
  .pipe(source('./app.js'))
  .pipe(buffer())
  .pipe(sourcemaps.init({loadMaps: true}))
    // Add transformation tasks to the pipeline here.
    .pipe(uglify())
    .on('error', gutil.log)
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest('./dist/js/'))
  .pipe(browserSync.stream());
});

/* Tarea que minifica las imagenes */

gulp.task('minified-assets-images', function() {
  return gulp.src(assetsImg)
  .pipe(imagemin({
    progressive: true
  }))
  .pipe(gulp.dest('./dist/images/'))
});

/*
 *  Tareas de Producción
 */

/* Tarea para gestionar los includes de html sin minificado */

gulp.task('include-files-prod', function() {
  return gulp.src(developPath)
  .pipe(fileinclude({
    prefix: '@@',
    basepath: '@file'
  }))
  .pipe(htmlmin({collapseWhitespace: false}))
  .pipe(gulp.dest('./dist/'))
});

/* Tarea que genera el css a traves de sass sin mapeos */

gulp.task('compile-sass-prod', function() {
  return gulp.src(sassPath)
  .pipe(sass().on('error', sass.logError))
  .pipe(postcss([autoprefixer(), cssnano()]))
  .pipe(gulp.dest('./dist/css/'))
});

/* Tarea que transpila el javascript para navegador con browserify sin mapeo */

gulp.task('browserify-js-files-prod', function() {
  let b = browserify({
    entries: './src/js/app.js',
    debug: true
  })
  .transform(babelify);

  return b.bundle()
  .pipe(source('./app.js'))
  .pipe(buffer())
    // Add transformation tasks to the pipeline here.
    .pipe(uglify())
    .on('error', gutil.log)
  .pipe(gulp.dest('./dist/js/'))
  .pipe(browserSync.stream());
});

/* Tarea que elimina los sourcemaps restantes para produccion */

gulp.task('clean-source-maps', function () {
    return gulp.src('dist/**/*.map', {read: false})
        .pipe(clean());
});


gulp.task('develop', [
  'include-files',
  'copying-fonts',
  'compile-sass',
  'browserify-js-files'], function () {

    browserSync.init({
      server: {
        baseDir: './dist/'
      }
    });
    // Define the watchers
    gulp.watch(includePath, ['include-files']);
    gulp.watch(sassPath, ['compile-sass']);
    gulp.watch('./src/js/**/*.js', ['browserify-js-files']);
    gulp.watch(assetsImg, ['minified-assets-images']);
    gulp.watch(developPath, ['include-files']).on('change', browserSync.reload);
  });

gulp.task('production', [
  'clean-source-maps',
  'include-files-prod',
  'copying-fonts',
  'compile-sass-prod',
  'browserify-js-files-prod',
  'minified-assets-images'], function () {
    console.log('Statics done');
  });


/* Tarea que elimina los sourcemaps restantes para produccion */

gulp.task('clean-source-maps-django', function () {
  return gulp.src('./../wambo/static/**/*.map', {read: false})
    .pipe(clean());
});

/* Tarea que genera el css a traves de sass sin mapeos */

gulp.task('compile-sass-django', function() {
  return gulp.src(sassPath)
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', sass.logError))
  .pipe(postcss([autoprefixer(), cssnano()]))
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest('./../wambo/static/css/'))
});

/* Tarea que transpila el javascript para navegador con browserify sin mapeo */

gulp.task('browserify-js-files-django', function() {
  let b = browserify({
    entries: './src/js/app.js',
    debug: true
  })
  .transform(babelify);

  return b.bundle()
  .pipe(source('./app.js'))
  .pipe(buffer())
  .pipe(sourcemaps.init({loadMaps: true}))
    // Add transformation tasks to the pipeline here.
    .pipe(uglify())
    .on('error', gutil.log)
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest('./../wambo/static/js/'))
  .pipe(browserSync.stream());
});

/* Tarea que minifica las imagenes */

gulp.task('minified-assets-images-django', function() {
  return gulp.src(assetsImg)
  .pipe(imagemin({
    progressive: true
  }))
  .pipe(gulp.dest('./../wambo/static/images/'))
});

/* Tarea para copiar las fuentes a dist/ */

gulp.task('copying-fonts-django', function() {
  return gulp.src(fontPath)
  .pipe(gulp.dest('./../wambo/static/fonts/'));
});

gulp.task('django-statics', [
  'clean-source-maps-django',
  'copying-fonts-django',
  'compile-sass-django',
  'browserify-js-files-django'], function () {
    console.log('Statics django done');
  });
