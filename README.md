
# Repositorio de Frontend

En este repositorio se encuentra todo el contenido estatico de la web

## Instalación

Ìnstalación de dependecias

```
npm install
```

Instalación de gulp

```
ǹpm install gulp-cli -g
```

Ejecutar en modo desarrollo

```
// En caso de no tener instalado gulp y para compartir la misma versión de gulp (RECOMENDADA)

npm run gulp-develop

gulp develop
```

Ejecutar en modo productivo

```
// En caso de no tener instalado gulp y para compartir la misma versión de gulp (RECOMENDADA)

npm run gulp-production

gulp production
```

## Estructura de directorios

#### dist

Archivos de html unificados js transpilados y scss compilado a css.

#### src

Archivos para el desarrollo

### Arrancar en Django

Ejecutar los siguientes comandos dentro de la carpeta frontend

```
npm install

npm run gulp-django-statics
```