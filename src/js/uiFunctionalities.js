'use strict'
const $ = require('jquery');

$('#searchButton').on('click', function (ev) {
  ev.preventDefault();
  $('body').attr('class','search-story');
});

$('#closeSearchBar').on('click', function (ev) {
  ev.preventDefault();
  $('body').attr('class','full-web');
});

$('#searchButtonDesktop').on('click', function (ev) {
  ev.preventDefault();
  let textInput = $('#navbarSearchInput').val(),
    textInputClass = $('#navbarSearchInput').attr('class');
  if (textInput !== '' && textInputClass === 'text-search-input active') {
    console.log('peticion ajax');
  }
  else {
    $('#navbarSearchInput').toggleClass('active');
  }
});

$('#comment-contanier').on('click', function (ev) {
  ev.preventDefault();
  $('#content-comment')[0].focus();
  return $('#comment-contanier')[0].className = 'comment-container open-form';
});

$('#cancel-comment').on('click', function (ev) {
  ev.preventDefault();
  ev.stopPropagation();
  console.log('wat');
  return $('#comment-contanier')[0].className = 'comment-container close-form';
});

$('#link-to-comment').on('click', function (ev) {
  ev.preventDefault();
  $('#commentMessage')[0].focus();
});

$('#goUpPage').on('click', function (ev) {
  ev.preventDefault();
  $('html, body').animate({
    scrollTop: 0
  });
});

$(document).on('click', '.reply-button', function (ev) {
  ev.preventDefault();
  $(this).closest('.card').find('.reply-from').attr('class', 'comment-container reply-from open-form');
});

$(document).on('click', '.cancel-button', function (ev) {
  ev.preventDefault();
  $(this).closest('.comment-container').attr('class', 'comment-container reply-from close-form');
});

$(document).on('click', '.reply-caret', function (ev) {
  ev.preventDefault();
  const cardContainerClassName = $(this).closest('.card-container')[0].className;
  console.log(cardContainerClassName);
  if (cardContainerClassName.indexOf('close') !== -1) {
    $(this).closest('.card-container').attr('class', 'card-container open-replies');
  }
  else {
    $(this).closest('.card-container').attr('class', 'card-container close-replies');
  }
});
