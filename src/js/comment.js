'use strict'
const Handlebars = require('handlebars');
const $ = require('jquery');
const showdown = require('showdown');
const commentService = require('./utils/commentApi').service;
const formatDetail = require('./utils/commentApi').formatDetail;
const postApi = require('./utils/postApi').service;
const validHelpers = require('./utils/validHelpers');
const postDetail = document.getElementById('container-detail-post');
const commentList = document.getElementById('comment-list-container');
const contentComment = document.getElementById('content-comment');
const moment = require('moment');
const errorBlock = document.querySelector('#error-comment-message.block-content');
const errorText = document.querySelector('#error-comment-message .error-text');
const converter = new showdown.Converter();

let context = {};
let _processAjaxCall = true;

if (document.getElementById('main-list-posts') !== null) {
  $('#main-list-posts .paragraph-text').each(function (index) {
    $(this).html(converter.makeHtml($(this).html()));
  });
}
console.log($('#main-list-posts .paragraph-text'));
if (document.getElementById('content-markdown') !== null) {
  let dataMArkDown = document.getElementById('content-markdown').innerHTML;
  $('#content-markdown').html(converter.makeHtml(dataMArkDown));
}

$('.like-btn').on('click', function (ev) {
  ev.preventDefault();
  const currentValue = Number($('.count-likes').text());
  postApi.getLike(`/api/posts/${postDetail.dataset.idpost}/like/`)
  .then((data) => {
    if (postApi.isLike) {
      $('.count-likes').html('<i class="fa fa-heart" aria-hidden="true"></i>');
    } else {
      $('.count-likes').html('<i class="fa fa-heart-o" aria-hidden="true"></i>');
    }
  })
  .catch(err => console.log);
});

const source   = document.getElementById('comment-list-template').innerHTML;
const template = Handlebars.compile(source);

Handlebars.registerHelper('moment', (date) => (
  moment(date, "YYYYMMDD").fromNow()
));


if ($('#comment-list-container').offset() !== undefined) {
  $(document).on('scroll', function (ev) {
    const sectionContentWeb = $('#comment-list-container').offset().top;
    const scrollToCompare = $(window).scrollTop();
    const scrollBottom = $(window).scrollTop() + $(window).height();
    if (scrollToCompare <= sectionContentWeb && _processAjaxCall) {
      // loading
      _processAjaxCall = false;
      context = { loading: true };
      commentList.innerHTML = template(context);
      // get comments
      commentService.getComments('/api/comments/')
      .then(data => formatDetail(commentService.commentList))
      .then(dataFormated => {
        context = {
          data: dataFormated,
          noItems: null,
          loading: false,
          errorText: null,
        };
        commentList.innerHTML = template(context);
      })
      .catch(err => {
        if (commentService.status === 404) {
          context = {
            noItems: 404,
            errorText: 'No Comments',
            loading: null,
          };
          commentList.innerHTML = template(context);
        }
        else {
          context = {
            noItems: 500,
            errorText: 'Internal Server Error',
            loading: false,
          };
          commentList.innerHTML = template(context);
        }
      });
    }
  });
}

// coment detail
$(document).on('submit', '.comment-form', function (ev) {
  ev.preventDefault();
  const idComment = $(this).attr('data-idComment');
  const contentComment = $(this).find('.content-comment-child')[0];
  const errorChild = $(this).find('.block-content-child')[0];
  const errorText = $(this).find('.error-text')[0];
  if (contentComment.innerHTML === '' || contentComment.innerHTML === undefined) {
    validHelpers.errorSubmit('Complete comment field', errorChild, errorText);
    validHelpers.helperValidity(contentComment, ev);
    return false;
  }
  const body = {
    content: contentComment.innerHTML
  };
  context = { loading: true };
  commentList.innerHTML = template(context);
  commentService.postComment(`/api/comments/create/?type=post&pk=${postDetail.dataset.idpost}&parent_id=${idComment}`, body)
  .then(data => commentService.getComments('/api/comments/'))
  .then(data => formatDetail(commentService.commentList))
  .then(dataFormated => {
    context = {
      data: dataFormated,
      noItems: null,
      loading: false,
      errorText: null,
    };
    commentList.innerHTML = template(context);
    _processAjaxCall = false;
  })
  .catch(err => {
    _processAjaxCall = false;
    if (commentService.status === 404) {
      context = {
        noItems: 404,
        errorText: 'No Comments',
        loading: null,
      };
      commentList.innerHTML = template(context);
    }
    else {
      context = {
        noItems: 500,
        errorText: 'Internal Server Error',
        loading: false,
      };
      validHelpers.errorSubmit('Internal server Error', errorBlock, errorText);
      validHelpers.helperValidity(contentComment, ev);
      commentList.innerHTML = template(context);
    }
  });
});
// coment form
$('#main-comment-form').on('click', function(ev) {
  ev.preventDefault();
  if (contentComment.innerHTML === '' || contentComment.innerHTML === undefined) {
    validHelpers.errorSubmit('Complete comment field', errorBlock, errorText);
    validHelpers.helperValidity(contentComment, ev);
    return false;
  }
  const body = {
    content: contentComment.innerHTML
  };
  context = { loading: true };
  commentList.innerHTML = template(context);
  commentService.postComment(`/api/comments/create/?type=post&pk=${postDetail.dataset.idpost}`, body)
  .then(data => commentService.getComments('/api/comments/'))
  .then(data => formatDetail(commentService.commentList))
  .then(dataFormated => {
    context = {
      data: dataFormated,
      noItems: null,
      loading: false,
      errorText: null,
    };
    commentList.innerHTML = template(context);
    _processAjaxCall = false;
  })
  .catch(err => {
    _processAjaxCall = false;
    if (commentService.status === 404) {
      context = {
        noItems: 404,
        errorText: 'No Comments',
        loading: null,
      };
      commentList.innerHTML = template(context);
    }
    else {
      context = {
        noItems: 500,
        errorText: 'Internal Server Error',
        loading: false,
      };
      validHelpers.errorSubmit('Internal server Error', errorBlock, errorText);
      validHelpers.helperValidity(contentComment, ev);
      commentList.innerHTML = template(context);
    }
  });
});
