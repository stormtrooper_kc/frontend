/**
 * Created by Juan on 22/4/17.
 */
//Cargar post de home
const $ =  require('jquery');
const moment = require('moment');
//var utils = require('./utils');

module.exports = {
    loadPost: function () {
        //Petición AJAX para cargar lista de post
        $.ajax({
            type: 'GET',
            url: 'http://localhost:3004/posts/',
            dataType: 'json',
            context: document.body,
            success: function(data){
                let html;
                if (data.length == 0){
                    console.log(data);
                    html ='No more post...';
                    $('.post-list').append(html);

                }else {
                    $('.post-list').html(''); // Clean comments
                    for(let i in data){
                        let post = data[i];
                        let title =  post.title  || "";
                        let media = post.media || "";
                        let author = post.author.name || "";
                        let timeAgo = post.publicationDate || "";
                        let category = post.categories || "";
                        let avatar = post.author.avatar;
                        let id = post.id || "";

                        html =`
                        <div class="post">
                            <div class="image-fixed">
                                <a href="detail.html">
                                    <img src="${media}" alt="blog-img">
                                </a>
                            </div>
                            <div>
                                <h3 id="title" class="html-text ">
                                        <a href="#">${title}</a>
                                </h3>
                            </div>
                            <div>
                                <p class="category">
                                    <span class="">
                                        <a href="#">${category[0].name}</a>
                                    </span>
                                </p>
                            </div>  
                            <div class="metadata">
                                <div class="avatar">
                                        <img src="${avatar}" alt=""> 
                                </div>
                                <div class="author">
                                <h4>${author}</h4>
                                    <span class="date">
                                        <time class="timeago" datetime="${timeAgo}">18th June, 2016</time>
                                    </span>
                                </div>

                            </div>
                        </div>`;
                        $('.post-list').append(html); // Load post
                    }
                }

            },
            error:function (data) {
                const html ='Oooops! Something went wrong. Try again later.';
                $('.post-list').append(html);

            }
        });

    }
}

/* LIKE Y COMMENT p
    <ul class="col-lg-12 html-text">
    <li>
    <a class="col-lg-6" data-article-id="${id}"> <i class="small material-icons">star</i> </a>
    </li>
    <li>
    <a class="col-lg-6"> <i class="small material-icons">comment</i> </a>
    </li>
    </ul>
*/
