'use strict'
const axios = require('axios');
const Q = require('q');

var service = {
  status: null,
  login: (uri, body) => {
    const d = Q.defer();
    axios.post(uri, body)
    .then(response => {
      d.resolve('success login');
    })
    .catch(err => {
      service.status = err.response.status;
      d.reject('Error login');
    });
    return d.promise;
  },
  signup: (uri, body) => {
    const d = Q.defer();
    axios.post(uri, body)
    .then(response => {
      d.resolve('success register');
    })
    .catch(err => {
      service.status = err.response.status;
      d.reject('Error reject');
    });
    return d.promise;
  }
};

module.exports = service;
