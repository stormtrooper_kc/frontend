'use strict'
const axios = require('axios');
const Q = require('q');
require('babel-polyfill');
const getCookie = require('./formHelper').getCookie;

const csrftoken = getCookie('csrftoken');

const formatDetail = async (array) => {
  let arrLen = array.length;
  let formatArray = [];
  while(arrLen--) {
    try {
      const response = await service.getRepplies(array[arrLen].url);
      const formatJson = {
        user: response.user.username,
        content: array[arrLen].content,
        id: array[arrLen].id,
        timestamp: array[arrLen].timestamp,
        repliesCount: array[arrLen].reply_count,
        replies: response.replies,
      };
      formatArray = formatArray.concat(formatJson);
    } catch(err) {
      throw `Error ${response}`;
    }
  }
  formatArray.sort((a, b) => {
    if(a.timestamp < b.timestamp) return true;
    return false;
  });
  return formatArray;
};

const service = {
  status: null,
  commentList: [],
  repliesData: [],
  getComments: (uri) => {
    const d = Q.defer();
    axios.get(uri)
    .then(response => {
      service.commentList = response.data.results;
      service.status = response.status;
      d.resolve('success get comment');
    })
    .catch(err => {
      service.status = err.response.status;
      d.reject('Error get comment');
    });
    return d.promise;
  },
  getRepplies: (uri) => {
    const d = Q.defer();
    axios.get(uri)
    .then(response => {
      service.repliesData = response.data;
      service.status = response.status;
      d.resolve(response.data);
    })
    .catch(err => {
      service.status = err.response.status;
      d.reject('Error get repplies');
    });
    return d.promise;
  },
  postRepplies: (uri, body) => {
    const d = Q.defer();
    axios.post(uri, body, {
      headers: {
        'X-CSRFToken': csrftoken,
      }
    })
    .then(response => {
      service.status = response.status;
      d.resolve('success comment');
    })
    .catch(err => {
      service.status = err.response.status;
      d.reject('Error comment');
    });
    return d.promise;
  },
  postComment: (uri, body) => {
    const d = Q.defer();
    axios.post(uri, body, {
      headers: {
        'X-CSRFToken': csrftoken,
      }
    })
    .then(response => {
      service.status = response.status;
      d.resolve('success comment');
    })
    .catch(err => {
      service.status = err.response.status;
      d.reject('Error comment');
    });
    return d.promise;
  }
};

module.exports = {
  service,
  formatDetail,
};
