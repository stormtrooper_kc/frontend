'use strict'
const axios = require('axios');
const Q = require('q');
const getCookie = require('./formHelper').getCookie;

const csrftoken = getCookie('csrftoken');

const service = {
  status: null,
  isLike: null,
  isUpdated: null,
  getLike: (uri) => {
    const d = Q.defer();
    axios.get(uri, {
      headers: {
        'X-CSRFToken': csrftoken,
      }
    })
    .then(response => {
      service.isLike = response.data.like;
      service.isUpdated = response.data.updated;
      service.status = response.status;
      d.resolve('success like');
    })
    .catch(err => {
      service.status = err.response.status;
      d.reject('Error like');
    });
    return d.promise;
  },
};

module.exports = {
  service,
};
