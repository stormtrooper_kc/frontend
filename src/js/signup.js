'use strict'
const $ = require('jquery');
const validHelpers = require('./utils/validHelpers');
const accessService = require('./utils/accessApi');
const usernameInput = document.getElementById('username-signup');
const emailConfirmInput = document.getElementById('email-confirm-signup');
const emailInput = document.getElementById('email-signup');
const pwdInput = document.getElementById('password-signup');
const agreementInput = document.getElementById('agreement-sign-up');
const signUpForm = $('#signup-form');
const inputs = $('#firstName-signup, #email-confirm-signup, #email-signup, #password-signup');
const errorBlock = document.querySelector('#error-message-signup.block-content');
const errorText = document.querySelector('#error-message-signup .error-text');
const signUpModal = $('#sign-up-modal');

signUpModal.on('hide.bs.modal', function () {
  inputs.each(function (index, obj) {
    obj.style.borderColor = '#ccc';
    obj.value = '';
  });
  signUpForm.removeClass('loading');
  agreementInput.checked = false;
});

signUpForm.on('submit', function (evt) {
  evt.preventDefault();
  if (usernameInput.checkValidity() === false) {
    validHelpers.errorSubmit('Complete first name field', errorBlock, errorText);
    validHelpers.helperValidity(usernameInput, evt);
    return false;
  }
  if (emailInput.checkValidity() === false) {
    if(emailInput.validity.typeMismatch === true || emailInput.validity.patternMismatch == true) {
      validHelpers.errorSubmit('Set up a correct format email', errorBlock, errorText);
      validHelpers.helperValidity(emailInput, evt);
      return false;
    }
      
    validHelpers.errorSubmit('Complete email field', errorBlock, errorText);
    validHelpers.helperValidity(emailInput, evt);
    return false;
  }

  if (emailConfirmInput.checkValidity() === false) {
    if(emailConfirmInput.validity.typeMismatch === true || emailConfirmInput.validity.patternMismatch == true) {
      validHelpers.errorSubmit('Set up a correct format email', errorBlock, errorText);
      validHelpers.helperValidity(emailConfirmInput, evt);
      return false;
    }
      
    validHelpers.errorSubmit('Complete email field', errorBlock, errorText);
    validHelpers.helperValidity(emailConfirmInput, evt);
    return false;
  }

  if (emailConfirmInput.value !== emailInput.value) {
    validHelpers.errorSubmit('Password don\'t match', errorBlock, errorText);
    validHelpers.helperValidity(emailConfirmInput, evt);
    return false;
  }

  if (pwdInput.checkValidity() === false) {
    validHelpers.errorSubmit('Complete password field', errorBlock, errorText);
    validHelpers.helperValidity(pwdInput, evt);
    return false;
  }
  if (agreementInput.checkValidity() === false) {
    validHelpers.errorSubmit('Please check the terms', errorBlock, errorText);
    validHelpers.helperValidity(agreementInput, evt);
    return false;
  }
  
  signUpForm.addClass('loading');
  const body = {
    username: usernameInput.value,
    email2: emailConfirmInput.value,
    email: emailInput.value,
    password: pwdInput.value
  };

  accessService.signup('/api/users/register/', body)
  .then(() => {
    signUpForm.removeClass('loading');
    location.reload();
  })
  .catch(err => {
    console.log(err);
    signUpForm.removeClass('loading');
    if (accessService.status === 400) {
      validHelpers.errorSubmit('Bad Request', errorBlock, errorText);
    }
    else if (accessService.status === 404) {
      validHelpers.errorSubmit('Url request Not Found', errorBlock, errorText);
    }
    else {
      validHelpers.errorSubmit('Internal server Error', errorBlock, errorText);
    }
  });

});

inputs.on('keydown', function (ev) {
  $(this)[0].style.borderColor = '#ccc';
});