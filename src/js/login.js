'use strict'
const $ = require('jquery');
const validHelpers = require('./utils/validHelpers');
const accessService = require('./utils/accessApi');
const usernameInput = document.getElementById('username-modal');
const emailInput = document.getElementById('email-modal');
const pwdInput = document.getElementById('password-modal');
const loginForm = $('#login-form');
const inputs = $('#email-modal, #password-modal, #username-modal');
const errorBlock = document.querySelector('#error-message.block-content');
const errorText = document.querySelector('#error-message .error-text');
const signInModal = $('#sign-in-modal');

signInModal.on('hide.bs.modal', function () {
  inputs.each(function (index, obj) {
    obj.style.borderColor = '#ccc';
    obj.value = '';
  });
  loginForm.removeClass('loading');
});

loginForm.on('submit', function (evt) {
  evt.preventDefault();
  if (usernameInput.checkValidity() === false) {
    validHelpers.errorSubmit('Complete username field', errorBlock, errorText);
    validHelpers.helperValidity(usernameInput, evt);
    return false;
  }
  if (emailInput.checkValidity() === false) {
    if(emailInput.validity.typeMismatch === true || emailInput.validity.patternMismatch == true) {
      validHelpers.errorSubmit('Set up a correct format email', errorBlock, errorText);
      validHelpers.helperValidity(emailInput, evt);
      return false;
    }
      
    validHelpers.errorSubmit('Complete email field', errorBlock, errorText);
    validHelpers.helperValidity(emailInput, evt);
    return false;
  }
  if (pwdInput.checkValidity() === false) {
    validHelpers.errorSubmit('Complete password field', errorBlock, errorText);
    validHelpers.helperValidity(pwdInput, evt);
    return false;
  }

  loginForm.addClass('loading');
  const body = {
    email: emailInput.value,
    password: pwdInput.value,
    username: usernameInput.value
  };

  accessService.login('/api/users/login/', body)
  .then(() => {
    loginForm.removeClass('loading');
    location.reload();
  })
  .catch(err => {
    loginForm.removeClass('loading');
    if (accessService.status === 400) {
      validHelpers.errorSubmit('Bad Request', errorBlock, errorText);
    }
    else if (accessService.status === 404) {
      validHelpers.errorSubmit('Url request Not Found', errorBlock, errorText);
    }
    else {
      validHelpers.errorSubmit('Internal server Error', errorBlock, errorText);
    }
  });

});

inputs.on('keydown', function (ev) {
  $(this)[0].style.borderColor = '#ccc';
});