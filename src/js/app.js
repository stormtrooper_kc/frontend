// To use bootstrap js
//global.jQuery = require('jquery');
//var $ = require('jquery');
//require('bootstrap');

require('./lib');
require('./uiFunctionalities');
require('./navbarBehavior');
require('./login');
require('./signup');
require('./comment');
require('./post-list');
require('./init');